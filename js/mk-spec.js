(function() {
  "use strict";
  /*global MK*/
  
  var getFormats = function getFormats() {
    var Formats = {
      acmonth: {
        reg: /^Za období: (\d{2}) \- (\d{4})$/,
        regmsg: 'Za období: datum ve formátu [mm - 20rr], kde m je mm je měsíc 01-12 a 20rr je rok 2000-2099'
      },
      acflow: {
        reg: new RegExp('^((\\-|)\\d{1,3})(\\' + MK.thSep + '\\d{3})*?(,\\d{2})$'),
        regmsg: 'Znaménko - pro záporné, číslo pro částku v korunách s oddělovačem tísíců (' + MK.thSepName + '), zakončeno čárkou a dvoučíslím haléřů'
      },
      ackcnum: {
        reg: new RegExp('^((\\-|)\\d{1,3})(\\' + MK.thSep + '\\d{3})*?(,\\d{2})(\\xa0| )' + MK.curSym + '( |)$'),
        regmsg: 'Číslo pro částku v korunách s oddělovačem tísíců, následované čárkou, dvoučíslím haléřů, mezerou a ' + MK.curSym
      },
      acrange: {
        reg: /Za období\:[^\d]+(\d\d)\.(\d\d)\.(20\d{2}) \-[^\d]+(\d\d)\.(\d\d)\.(20\d{2})/m,
        regmsg: 'Za období: datum ve formátu [dd.mm.20rr - dd.mm.20rr], kde dd je den 01-31, mm je měsíc 01-12 a 20rr je rok 2000-2099'
      },
      number: {
        reg: /^\d{1,12}$/,
        regmsg: '1 - 12 místné kladné číslo'
      },
      text: {
        reg: /^(\w|\W)+$/,
        regmsg:'Jakýkoliv text obsahující tisknutelné znaky'
      },
      truedate: {
        reg: /^(\d{2})\.(\d{2})\.(\d{4})$/,
        regmsg: 'Datum ve formátu dd.mm.yyyy'
      }
    };
    return Formats;
  };
  
  /*Statické základní DOM query elementy*/
  //obdobi výpisu z účtu specifické pro druh výpisu
  var BQuery = {};
  BQuery.range = {
    acmonth: 'body>div:nth-child(1)>table:nth-child(4)>tbody:nth-child(1)>tr:nth-child(3)>td:nth-child(1)',
    acrange: 'body>div:nth-child(1)>table:nth-child(1)>tbody:nth-child(1)>tr:nth-child(1)>td:nth-child(1)>table:nth-child(8)>tbody:nth-child(1)>tr:nth-child(1)>td:nth-child(1)'
  };
  /*Hlavní tabulky*/
  //sumarizace výpisu
  BQuery.sum = {
    acmonth: 'body>div:nth-child(1)>table:nth-child(4)>tbody:nth-child(1)>tr:nth-child(9)>td:nth-child(1)>table:nth-child(2)>tbody:nth-child(1)',
    acrange: 'body>div:nth-child(1)>table:nth-child(1)>tbody:nth-child(1)>tr:nth-child(1)>td:nth-child(1)>table:nth-child(12)>tbody:nth-child(1)'
  };
  //transakce
  BQuery.trans = {
    acmonth: 'body>div>table:nth-child(4)>tbody>tr:nth-child(10)>td>table>tbody',
    acrange: 'body>div>table:nth-child(1)>tbody>tr:nth-child(1)>td>table:nth-child(15)>tbody'
  };
  
  var Spec = null;
  var sidx = 0;
  MK.resetSpec = function resetSpec() {MK.stType = null; Spec = null; sidx = 0;};
  
  /*vyrobí nový prázdný spec objekt*/
  MK.getSpec = function getSpec() {
    
    var spec = {
      statik: {
        'Za období': {
          query: BQuery.range[MK.stType],
          perms: 'write',
          ioname: MK.stType
        },
        
        'Počáteční zůstatek': {
          query: BQuery.trans[MK.stType] + '>tr:nth-child(1) > td:nth-child(2)>div>b',
          perms: 'write',
          ioname: 'ackcnum'
        },
        
        'Konečný zůstatek': {
          query: BQuery.trans[MK.stType] + '>tr:last-child>td:nth-child(2)>div>b',
          perms: 'read',
          ioname: 'ackcnum'
        },
        
        'Kreditní položky' : {
          'Počet operací': {
            query: BQuery.sum[MK.stType] + '>tr:nth-child(2)>td:nth-child(2)',
            perms: 'read',
            ioname: 'number'
          },
          'Částka' : {
            query: BQuery.sum[MK.stType] + '>tr:nth-child(2)>td:nth-child(3)',
            perms: 'read',
            ioname: 'ackcnum'
          }
        },
        
        'Debetní položky' : {
          'Počet operací': {
            query: BQuery.sum[MK.stType] + '>tr:nth-child(3)>td:nth-child(2)',
            perms: 'read',
            ioname: 'number'
          },
          'Částka' : {
            query: BQuery.sum[MK.stType] + '>tr:nth-child(3)>td:nth-child(3)',
            perms: 'read',
            ioname: 'ackcnum'
          }
        },
        
        'Součet': {
          'Počet operací': {
            query: BQuery.sum[MK.stType] + '>tr:nth-child(4)>td:nth-child(2)',
            perms: 'read',
            ioname: 'number'
          },
          'Částka' : {
            query: BQuery.sum[MK.stType] + '>tr:nth-child(4)>td:nth-child(3)',
            perms: 'read',
            ioname: 'ackcnum'
          }
        }
      },
      
      dynamic: {
        query: BQuery.trans[MK.stType] + '>tr:nth-child(n+3):nth-last-child(n+2)'
      }
    };
    
    spec.dynamic.transakce = {};
    if(MK.stType === 'acmonth') {
      spec.dynamic.transakce['Číslo dokladu'] = {
        query: 'td:nth-child(' + (++sidx) + ') > nobr',
        perms: 'write',
        ioname: 'number'
      };
      sidx = 1;
    } else {
      sidx = 0;
    }
    spec.dynamic.transakce['Datum uskutečnění transakce'] = {
      query: 'td:nth-child(' + (sidx + 1) + ') > nobr',
      perms: 'write',
      ioname: 'truedate'
    };
    spec.dynamic.transakce['Datum zaúčtování transakce'] = {
      query: 'td:nth-child(' + (sidx + 2) + ') > nobr',
      perms: 'write',
      ioname: 'truedate'
    };
    spec.dynamic.transakce.Podrobnosti = {
      query: 'td:nth-child(' + (sidx + 3) + ')',
      perms: 'write',
      ioname: 'text'
    };
    spec.dynamic.transakce['Částka transakce'] = {
      query: 'td:nth-child(' + (sidx + 4) + ') > nobr',
      perms: 'write',
      ioname: 'acflow'
    };
    spec.dynamic.transakce['Účetní zůstatek po transakci'] = {
      query: 'td:nth-child(' + (sidx + 5) + ') > nobr',
      perms: 'read',
      ioname: 'acflow'
    };
    
    /*help Funkce -> test je li non array objekt, použito dále v setNames*/
    var isOb = function isOb(o) {
      return (typeof o === 'object' && o.constructor === Object);
    };
    
    /*přidá string vlastnost name a parent do objektů*/  
    var setNames = function setNames(o, rootname) {
      o.name = o.name || rootname || 'root';
      for(var n in o) {
        if (isOb(o[n])) {
          o[n].name = n;
          o[n].parent = o.name;
          setNames(o[n]);
        }
      }
      return o;
    };
    
    Spec = Spec || setNames(spec, 'Specifikace');
    Spec.Formats = getFormats();
    return Spec;
  };
})();