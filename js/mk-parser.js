(function() {
  "use strict";
  /*global MK*/
  /******** Model parser ********************************************************/
  MK.Parser = function() {
    /*logovani*/
    this.log = [];
    this.errors = 0;
  };
  
  MK.Parser.prototype.logger = function logger(logob) {
    var log = {
      type: logob.type,
      text: logob.text
    };
    this.log.push(log);
    return log;
  };
  
  MK.Parser.prototype.parseOne = function parseOne(onespecob, eldom) {
    //console.log('Parsuji:', onespecob, ' element DOM:', eldom);
    onespecob.domel = eldom.querySelector(onespecob.query); //vyplni dom objekt
    if (onespecob.domel) {
      //TD MK.Util.nodeLoad(domel)
      onespecob.value = onespecob.domel.innerHTML; //ziskej content z dom
      /*dopln smajlíka, když není text*/
      //TD MK.Util.nodeLoad(domel)
      if(onespecob.value.length === 0) {
        onespecob.value = onespecob.domel.innerHTML = '☹';
      }
      var validate = MK.FormatValidators.validate(onespecob, eldom); //zvaliduj
      onespecob.log = []; //individuální spec log array
      for (var i = 0; i < validate.log.length; i++) {
        onespecob.log.push(this.logger(validate.log[i]));
      }
      onespecob.valid = validate.valid;
      this.view.renderOneResult(onespecob, onespecob.domel); //změny ve view
      if (onespecob.valid) {
        MK.Io.save(onespecob);
      } else {
        this.errors++; //nevalidní > 0
      }
    } else {
      var keyname = (onespecob.parent) ? onespecob.parent + '->' + onespecob.name : onespecob.name;
      this.view.renderLog({type: 'err', text: 'Nenalezen DOM objekt element pro: ' + keyname + ' !'});
      this.errors++; //nevalidní > 0
    }
  };
  
  MK.Parser.prototype.parseDOM = function parseDOM() {
    this.errors = 0; //vynuluje počítadlo chyb
    this.view.clearEls(['logerr', 'logmsg']);
    var base = ['Za období', 'Počáteční zůstatek', 'Konečný zůstatek'];
    var sumary = ['Kreditní položky', 'Debetní položky', 'Součet'];
    var ifrDoc = this.view.ifr.contentDocument;
    var docBody = ifrDoc.body;

    if(typeof MK.stType === 'string' && (MK.stType === 'acmonth' || MK.stType === 'acrange')) {
      var statik = MK.getSpec().statik;
      var dynamic = MK.getSpec().dynamic;
      var iter = MK.getSpec().dynamic.transakce;
      
      /*Statické - jedináčci */
      for (var i = 0; i < base.length; i++) {
       this.parseOne(statik[base[i]], docBody);
      }
      /*Statické co mají subelementy 2 úrovně*/
      for (var j = 0; j < sumary.length; j++) {
        var sum = statik[sumary[j]]; //rodiče
        for (var c in sum) { //děti
          if(typeof sum[c] === 'object' && sum[c].constructor === Object) {
            this.parseOne(sum[c], docBody);
          }
        }
      }
      
      /*Ziskat dynamické položky <tr> z tabulky výpisů a nasypat je do this.parseFlow*/
      var flowRows = docBody.querySelectorAll(dynamic.query);
      dynamic.flows = []; /*vynulujeme obsah předchozích  parsovaných řádků*/
      /*projdeme položky podobjektu dynamic*/
      for (var k = 0; k < flowRows.length; k++) {
        /*naklonování stromu iter (transakce)*/
        var trOb = this.objClone(iter);
        for (var so in trOb) { //jednotlive bunky
          if (typeof trOb[so] === 'object' && trOb[so].constructor === Object) {
            trOb[so].flowIndex = k;
            this.parseOne(trOb[so], flowRows[k]);
          }
        }
        dynamic.flows.push(trOb);
        flowRows[k].setAttribute('data-flow', 'true');//značení tr výpisu
      }
      console.log('MK DOM parsed for title ->', ifrDoc.title, ',', this.errors, 'errors.');
    } else {
      this.errors++;
      this.view.renderLog({type: 'err', text: 'Nepodařilo se určit druh bankovního výpisu dle tittle tagu HTML dokumentu!'});
      console.log('MK DOM parse failed: No account statement type detected!', this.errors, 'errors.');
    }
  };
  
  MK.Parser.prototype.recalculate = function recalculate() {
    //výpočty budou provedeny jen když nebudou chyby po parsování formátu
    if (this.errors === 0) {
      //rekalkulace všech tr tabulky výpisů a update DOM
      var spec = MK.getSpec(); var st = performance.now();
      if(Array.isArray(spec.dynamic.flows)) {
        if (spec.dynamic.flows.length > 0) {
          this.ValidRanges(spec.dynamic.flows); //pro LogicValidatory
          this.ValidAcDates(spec.dynamic.flows); //pro
          if (this.errors === 0) {
            this.sortByDate(spec.dynamic.flows); //seřazení dle data
            this.sumFlows(); //výpočty pohybů a souhrnu v modrých
            this.sortIds(spec.dynamic.flows); //přečisluje idčka
          }
        } else {
          this.zeroSumTable(); //nastaví nuly ve výsledcích sumarizace když nejsou žádné transakce
          spec.statik['Konečný zůstatek'].parsedValue = spec.statik['Počáteční zůstatek'].parsedValue;
          MK.Io.load(spec.statik['Konečný zůstatek']);
        }
        console.log('MK DOM recalculated', MK.getSpec(), 'at:', performance.now() - st, 'msec');
      }
    } else {
      console.log('MK DOM recalculate failed no dynamic.flows array!');
    }
  };
  
  MK.Parser.prototype.ValidRanges = function ValidRanges(flows) {
    for (var i = 0; i < flows.length; i++) {
      var specob = flows[i]; //spec
      /*parsované objekty datumů*/
      var datekeys =  ['Datum uskutečnění transakce', 'Datum zaúčtování transakce'];
      for (var j = 0; j < datekeys.length; j++) {
        /*validace parsovaných datumů*/
        var trdate = specob[datekeys[j]];
        var lvtrdate = MK.LogicValidators.rangedate(trdate);
        trdate.log.push(lvtrdate.log); //přidat do element logu
        trdate.valid = lvtrdate.valid;
        if (!lvtrdate.valid) { //nevalidní období
          this.errors++; //zvýšit counter chyb
          trdate.domel.setAttribute('data-status', 'error');
        }
      }
    }
  };
  
  MK.Parser.prototype.ValidAcDates = function ValidDates(flows) {
    for (var i = 0; i < flows.length; i++) {
      var specob = flows[i]; //spec
      var zdatespec = specob['Datum zaúčtování transakce'];
      var lvacd = MK.LogicValidators.acdate(specob);
      zdatespec.log.push(lvacd.log);
      zdatespec.valid = lvacd.valid;
      if (!lvacd.valid) {// nevalidní datum zaúčtování vůči datumu uskutečnění
        this.errors++;
        zdatespec.domel.setAttribute('data-status', 'error');
      }
    }
  };
  
  MK.Parser.prototype.sortByDate = function sortByDate(flows) {
    /*Seřazení flows dle data zaúčtování*/
    var comparefn = function(a, b) {
      var x = a['Datum zaúčtování transakce'].parsedValue.getTime();
      var y = b['Datum zaúčtování transakce'].parsedValue.getTime();
      //console.log(x, y, a['Datum zaúčtování transakce'].parsedValue, b['Datum zaúčtování transakce'].parsedValue);
      return x - y;
    };
    
    /*seřazení vzestupně dle data zaúčtování*/
    var sflows = MK.Util.msort(flows, comparefn);
    flows = sflows;

    /*Načtení tr elementů dle seřazené spec do pole trs a smazání původních v dom*/
    var trs = [];
    for (var i = 0; i < flows.length; i++) {
      var trel = flows[i]['Datum zaúčtování transakce'].domel.parentNode.parentNode;
      trs.push(trel);
      trel.parentNode.removeChild(trel); //vymaže z dom ale zůstanou načteny v trs[];
    }
  
    /*Vložení v novém pořadí*/
    var bfquery = MK.stType === 'acmonth' ? 
      'body>div>table:nth-child(4)>tbody>tr:nth-child(10)>td:nth-child(1)>table>tbody>tr:last-child' :
      'body>div>table:nth-child(1)>tbody>tr:nth-child(1)>td:nth-child(1)>table:nth-child(15)>tbody>tr:last-child';
    //var bfquery = getSpec().statik
    var bfel = this.view.ifr.contentDocument.querySelectorAll(bfquery)[0];
    for (var j = 0; j < trs.length; j++) {
      bfel.parentNode.insertBefore(trs[j], bfel);
      //udělat správné odsazení
      var indt = this.view.ifr.contentDocument.createTextNode('\n              ');
      bfel.parentNode.insertBefore(indt, bfel);
    }
    this.sortIds(flows);
  };
  
  MK.Parser.prototype.sortIds = function sortIds(flows) {
    /*Přečíslovat podle defId || prvni položky >>*/
    if(MK.stType === 'acmonth') { //jen při měsíčním výpisu!!!
      
      var firstID; //čislo dokladu prvno položky vypisu
      var defIDel = this.view.ifr.contentDocument.querySelector('[data-defid]');

      if (defIDel) {
        /*hodnota indexu id označeneho pro vychozi řazeni*/
        var specindex = parseInt(defIDel.getAttribute('data-defid'), 10);
        /*prvni je o index mensi, ale musi byt >= 0*/
        firstID = flows[specindex]['Číslo dokladu'].parsedValue - specindex;
        firstID = (firstID >= 0) ? firstID : 0; // korekce na 0 vyjde li firstID < 0
      } else {
        firstID = flows[0]['Číslo dokladu'].parsedValue; //bere z prvni položky
      }
      //var firstID = MK.getSpec().dynamic.flows[0]['Číslo dokladu'].parsedValue;
      for (var i = 0; i < flows.length; i++) {
        var spec = flows[i];
        spec['Číslo dokladu'].parsedValue = i + firstID;
        MK.Io.load(spec['Číslo dokladu']);
      }
    }
  };
  
  /*vynulovani sum elementu pro počátek výpočtů nebo při 0 pohybů v tabulce transakcí*/
  MK.Parser.prototype.zeroSumTable = function zeroSumTable() {
    var spec = MK.getSpec();
    var sumels = ['Kreditní položky', 'Debetní položky', 'Součet'];
    for (var x = 0; x < sumels.length; x++) {
      var parent = spec.statik[sumels[x]];
      for (var sp in parent) {
        var sob = parent[sp];
        if (typeof sob === 'object' && sob.constructor === Object) {
          parent[sp].parsedValue = 0;
          MK.Io.load(parent[sp]); //JETAm 0 !== true !!!!!!
        }
      }
    }
  };
  
  //výpočty číselných hodnot a dosazení do tabulky
  MK.Parser.prototype.sumFlows = function sumFlows() {
    var spec = MK.getSpec(); //načíst celou spec
    var flows = spec.dynamic.flows; //pohyby
    
    /*Vynulujeme sumarizační tabulku*/
    this.zeroSumTable();
    
    /*Srovnáme hodnotu konečného zůstatku na hodnotu počátečního*/
    spec.statik['Konečný zůstatek'].parsedValue = spec.statik['Počáteční zůstatek'].parsedValue;
    
    for (var i = 0; i < flows.length; i++) {
      var specob = flows[i]; //specob objekt řádku (elementu tr)
      /*Aktualizujeme hodnotu konečneho zustatku tim ze pricteme castku transakce v radku */
      spec.statik['Konečný zůstatek'].parsedValue += specob['Částka transakce'].parsedValue;
      /*Aktualizovanou hodnotu konecneho zustatku ulozime do zustatku po transakci*/
      specob['Účetní zůstatek po transakci'].parsedValue = spec.statik['Konečný zůstatek'].parsedValue;
      MK.Io.load(specob['Účetní zůstatek po transakci']); /*a nahrajeme ji do DOM*/
      /*vypoceteme hodnoty pro sum tabulku*/
      if (specob['Částka transakce'].parsedValue >= 0) { //kreditní transakce
        spec.statik['Kreditní položky']['Počet operací'].parsedValue++;
        spec.statik['Kreditní položky']['Částka'].parsedValue += specob['Částka transakce'].parsedValue;
      } else { //debetní transakce
        spec.statik['Debetní položky']['Počet operací'].parsedValue++;
        spec.statik['Debetní položky']['Částka'].parsedValue += specob['Částka transakce'].parsedValue;
      }
      /*všechny operace*/
      spec.statik['Součet']['Počet operací'].parsedValue++;
      spec.statik['Součet']['Částka'].parsedValue += specob['Částka transakce'].parsedValue;
    }
    
    // /*vyplněni konečneho zustatku do DOM*/
    spec.statik['Konečný zůstatek'].parsedValue = spec.statik['Konečný zůstatek'].parsedValue;
    MK.Io.load(spec.statik['Konečný zůstatek']);
    
    /*vyplneni sum elementu do dom*/
    var sumels = ['Kreditní položky', 'Debetní položky', 'Součet'];
    for (var p in sumels) {
      var par = spec.statik[sumels[p]];
      for (var s in par) {
        if (typeof par[s] === 'object' && par[s].constructor === Object) {
          MK.Io.load(par[s]);
        }
      }
    }
  };
  
  MK.Parser.prototype.objClone = function objClone(obj) {
    /*Univerzální metoda pro klonování jakéhokoliv JS objektu*/
    var out;

    if (typeof obj !== "object") { // String, Number, Boolean
      out = obj;
    } else if (typeof obj === 'object' && Array.isArray(obj)) { //Array
      out = [];
      for (var i = 0 ; i < obj.length; i++ ) { //klonování pole
        out.push( this.objClone(obj[i]) );
      }
    } else if (typeof obj === 'object' && obj.constructor === Date) { // Date
      out = new Date(obj);
    } else if (typeof obj === 'object' && obj.constructor === Node && obj.nodeName) { // DOM node
      out = obj;
    } else { // Objekt
      out = {};
      for (var o in obj) { //rekurze
        out[o] = this.objClone(obj[o]);
      }
    }
    return out;
  };
})();