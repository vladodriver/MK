(function() {
  "use strict";
  /*global MK*/
  MK.Util = {};
  
  /*zaokrouhlí float na num desetinných míst*/
  MK.Util.round = function round(float, num) {
    num = (parseInt(num, 10) > 0) ? parseInt(num, 10) : 2; //výchozí hod. = 2
    float = parseFloat(float);
    var x = Math.pow(10, num);
    return (Math.round(float * x) / x); 
  };
  
  /*Cross browser stable sort (msort) algo*/
  MK.Util.msort = function msort(array, cfn) {
		var defCfn = function(a,b) {return a - b;}; //default build in compare fn
    cfn = (typeof cfn === 'function') ? cfn : defCfn; //or use user defined

    var merge = function(left, right) {
      var result  = [],
      il = 0,
      ir = 0;

      while (il < left.length && ir < right.length) {
        if (cfn(left[il], right[ir]) <= 0) {
          result.push(left[il++]);
        } else {
          result.push(right[ir++]);
        }
      }
      return result.concat(left.slice(il)).concat(right.slice(ir));
    };

    var mergeSort = function(items){
      // Done 0 or 1 -> item arrays don't need sorting
      if (items.length < 2) {
        return items;
      }
			// recursive
      var middle = Math.floor(items.length / 2);
      var left = items.slice(0, middle);
      var right = items.slice(middle);
      
      return merge(mergeSort(left), mergeSort(right));
    };
    return mergeSort(array);
  };
  
  /*Fill zeros to string representation of number*/
  MK.Util.fillZero = function fillZero(val, num, side, char) {
    num = num || '2'; //default final length 2
    side = side || 'left'; //default side of adding
    char = char || '0'; //default fill 0 (zero) char
    
    var numval = val.toString(); 
    var numlen = numval.length;
    while(numlen < num) {
      if(side === 'left') {
        numval = char + numval;
      } else if (side === 'right') {
        numval = numval + char;
      }
      numlen = numval.length;
    }
    return numval;
  };
  
  /*rs object specifikace záměny více znaků, str text*/
  MK.Util.etRepl = function erRepl(rs, str) {
		for (var r in rs) {
			var reg = new RegExp(r);
			str = str.replace(reg, rs[r]);
		}
		return str;
	};

})();