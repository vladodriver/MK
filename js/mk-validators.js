(function() {
  /*validační funkce*/
  /*global MK*/
  "use strict";
  
  var getKeyName = function(specob) {
    return (specob.parent) ? specob.parent + ' → ' + specob.name : specob.name;
  };
  
  MK.FormatValidators = {
    /*Základní validace formátu jakékoliv položky*/
    validate: function validate(specob, docroot) {
      var logs = []; //pole pro výsledky
      var query = docroot.querySelectorAll(specob.query);
      var valid = true;
      /*Jednoznačná single odpověď*/
      var value = query[0].innerHTML;
      var reg = MK.getSpec().Formats[specob.ioname].reg;
      var regmsg = MK.getSpec().Formats[specob.ioname].regmsg;
      var match = reg.exec(value);
      if (match) {
        logs.push({type: 'msg', text: '<b>Položka:</b> ' + getKeyName(specob) + ' <b>je ve správném formátu:</b> ' + regmsg});
        //následuje validační funkce je li obsažena, jmenuje se stejně jako ioname a je zde v this
        if (typeof MK.FormatValidators[specob.ioname] !== 'undefined') {
          var specialvalidate = MK.FormatValidators[specob.ioname](specob, value);
          valid = specialvalidate.valid ? true : false;
          if (Array.isArray(specialvalidate.log)) {
            logs.concat(specialvalidate.log);
          } else {
            logs.push(specialvalidate.log);
          }
        }
      } else {
        logs.push({type: 'err', text: '<b>Položka:</b> ' + getKeyName(specob) + ' <b>nemá správný formát:</b> ' + regmsg});
        valid = false; //vrátit výsledek když neprojde validací formátu
      }
      return {valid: valid, log: logs};
    },
    
    /*Individual validatční funkce s názvem stejným jako specob.ioname*/
    truedate: function truedate(specob, value) {
      var reg = MK.getSpec().Formats.truedate.reg;
      var dmy = value.match(reg);
      var day = parseInt(dmy[1], 10);
      var mon = parseInt(dmy[2], 10) - 1; //mesice od nuly
      var year = parseInt(dmy[3], 10);
      var td = new Date(year, mon, day);
      
      var out;
      /*Kontrola platnosti datumu*/
      if (td.getDate() === day && td.getMonth() === mon && td.getFullYear() === year && year >= 2000 && year <= 2099) {
        out = {valid: true, log: {type: 'msg', text: '<b>Položka:</b> ' + getKeyName(specob) + ' <b>je:</b> v rozsahu let 2000 - 2099.'}};
      } else {
        console.log('CHYBA mon:', td.getMonth(), mon);
        out = {valid: false, log: {type: 'err', text: '<b>Položka:</b> ' + getKeyName(specob) + ' <b>není:</b> v rozsahu let 2000 - 2099!'}};
      }
      return out;
    }
  };
  
  MK.LogicValidators = {
    rangedate: function rangedate(specob) {
      /*kontrola jestli spadá do daného období*/
      var drob = MK.getSpec().statik['Za období'];
      var sobtime = specob.parsedValue.getTime();
      var start = drob.parsedValue.start.getTime();
      var end = drob.parsedValue.end.getTime();
      if (sobtime < start || sobtime > end) {
        return {valid: false, log: {type: 'err', text: '<b>Položka:</b> ' + getKeyName(specob) +
          ' </b>není:</b> platné datum pro účetní období výpisu: "' + drob.value + '" !'}};
      } else {
        return {valid: true, log: {type: 'msg', text: '<b>Položka:</b> ' + getKeyName(specob) +
          ' <b>je:</b> platné datum pro účetní období výpisu: "' + drob.value + '" .'}};
      }
    },
    
    acdate: function acdate(rowspecob) { 
      /*kontrola jestli datum zaúčtování transakce je větší v případě +transakce a obráceně v případě -*/
      var udate = rowspecob['Datum uskutečnění transakce'];
      var zdate = rowspecob['Datum zaúčtování transakce'];
      var udateval = udate.parsedValue.getTime();
      var zdateval = zdate.parsedValue.getTime();
      var kc = rowspecob['Částka transakce'].parsedValue;
      var out;
      //console.log('Udatum', udate, 'Zdatum', zdate);
      if (kc >= 0) {
        if (udateval < zdateval) {
          out = {valid: false, log: {type: 'err', text: '<b>Položka:</b> ' + getKeyName(zdate) +
          ' <b>nesmí být pozdější datum, než je</b> datum uskutečnění kreditní transakce (+) <b>hodnota:</b> ' + udate.value + ' !'}};
        } else {
          out = {valid: true, log: {type: 'msg', text: '<b>Položka:</b> ' + getKeyName(zdate) +
          ' <b>je dřívější, nebo stejné datum jako</b> datum uskutečnění kreditní transakce (+) <b>hodnota:</b> ' + udate.value + ' .'}};
        }
      } else if (kc < 0) {
        if (udateval > zdateval) {
          out = {valid: false, log: {type: 'err', text: '<b>Položka:</b> ' + getKeyName(zdate) +
          ' <b>nesmí být dřívější datum, než je</b> datum uskutečnění debetní transakce (-) <b>hodnota:</b> ' + udate.value + ' !'}};
        } else {
          out = {valid: true, log: {type: 'msg', text: '<b>Položka:</b> ' + getKeyName(zdate) +
          ' <b>je pozdější, nebo stejné datum, jako</b> datum uskutečnění debetní transakce (-) <b>hodnota:</b> ' + udate.value + ' !'}};
        }
      }
      return out;
    }
  };
  
})();