(function() {
  "use strict";
  /*global MK*/
  /*******třída z editačními funkcemi do view**********************************/
  MK.Editor = function Editor(hist) {
    this.hist = hist; //implementace historie
    
    this.editing = false;
    this.editingmode = null; //stav volby editačního modu
  };
  
  MK.Editor.prototype.emodes = {
    modify: {
      name: 'modify',
      msg: '<p class="tip">Kliknutím do zelených položek zahájíte modifikaci obsahu. Pro uplatnění změn použijte tlačítko <b>OK</b>, nebo opusťte editované pole.</p>'
    },
    del: {
      name: 'del',
      msg: '<p class="tip">Kliknutím do barevné položky transakce jí označíte pro smazání. Všechny označené smažete tlačítkem <b>OK</b>.</p>'
    },
    add: {
      name: 'add',
      msg: '<p class="tip">Kliknutí do barevné položky přidá novou transakci. Pořadí umístění se řídí datumem zaúčtování nejbližší předchozí. Změny potvrdíte tlačítkem <b>OK</b>.</p>'
    }
  };
  
  /*smazání všech atributů  v dokumentu výpisu*/
  MK.Editor.prototype.remAttrs = function remAttrs(attrs) {
    if (typeof attrs === 'object' && Array.isArray(attrs)) {
      for (var i = 0; i < attrs.length; i++) {
        this.remAttrs(attrs[i]);
      }
    } else if (typeof attrs === 'string') {
      var cels = this.view.ifr.contentDocument.querySelectorAll('[' + attrs + ']');
      for (var j = 0; j < cels.length; j++) {
        cels[j].removeAttribute(attrs);
      }
    }
  };
  
  /*Async vyhledání Objektu Spec kliknutého elementu - vola se cb*/
  MK.Editor.prototype.getSpecFromEl = function getSpecFromEl(fspec, el, cb) {
    for (var o in fspec) { //traversing
      if(fspec.hasOwnProperty(o) && typeof fspec[o] === 'object' && (Array.isArray(fspec[o]) || fspec[o].constructor === Object)) {
        if (o in fspec && typeof fspec[o].domel === 'object' && fspec[o].domel === el) {
          cb(fspec[o]);
        } else {
          this.getSpecFromEl(fspec[o], el, cb);
        }
      }
    }
  };
  
  MK.Editor.prototype.modify = function modify(el) {
    if(typeof el === 'object' && el && typeof el.cloneNode === 'function') {
      this.remAttrs('contenteditable'); //vypnout CE u ostatnich
      var datr = el.getAttribute('data-status');
      if (datr === 'write' || datr === 'error') {
        el.contentEditable = true;
        /*jestliže je editovaný element číslem dokladu -> označit ho jako výchozí pro výpočet*/
        this.getSpecFromEl(MK.getSpec(), el, function(spec) {
          if (spec.name === 'Číslo dokladu') {
            this.remAttrs('data-defid');
            spec.domel.setAttribute('data-defid', spec.flowIndex);
          }
        });
        el.focus();
      }
    }
  };
  
  MK.Editor.prototype.add = function add(el) {
    //find spec for click element and make new using dates and id above
    if(typeof el === 'object' && el && typeof el.cloneNode === 'function') {
      var self = this;
      this.getSpecFromEl(MK.getSpec(), el, function(spec) {
        var fullSpec = MK.getSpec();
        var flowEl, startdate, acdate, transId;
        var flows = MK.getSpec().dynamic.flows;
        var flowIdx = spec.flowIndex;
        
        if(typeof fullSpec.dynamic.flows[spec.flowIndex] !== 'undefined') {
          if(flows && typeof flows === 'object' && Array.isArray(flows) && flows.length > 0) {
            var flRows = self.view.ifr.contentDocument.querySelectorAll('[data-flow="true"]');
            flowEl = flRows[flowIdx] !== 'undefined' ? flRows[flowIdx] : null;
            startdate = flows[flowIdx]['Datum uskutečnění transakce'].value;
            acdate = flows[flowIdx]['Datum zaúčtování transakce'].value;
            transId = (MK.stType === 'acmonth') ? flows[flowIdx]['Číslo dokladu'].value : null;
          }
        } else {
          var stStart = MK.getSpec().statik['Počáteční zůstatek'];
          flowEl = stStart.domel.parentElement.parentElement.parentElement.nextElementSibling;
          //acmonth || acrange start date
          var sdate = MK.getSpec().statik['Za období'].parsedValue.start;
          startdate = acdate = MK.Util.fillZero(sdate.getDate().toString(), 2) + '.' +
            MK.Util.fillZero((sdate.getMonth() + 1).toString(), 2) + '.' + sdate.getFullYear();
          if(MK.stType === 'acmonth' && typeof flows[0] !== undefined && typeof flows[0]['Číslo dokladu']) {
            transId = flows[0]['Číslo dokladu'].parsedValue - 1;
            transId = transId > 1 ? transId : 1;
            transId = transId.toString();
          } else {
            transId = (MK.stType === 'acmonth') ? 1 : null;
          }
        }
        self.addNewFlowTrel(flowEl, startdate, acdate, transId);
      });
    }
  };
  
  MK.Editor.prototype.del = function del(el) {
    if(typeof el === 'object' && el && typeof el.cloneNode === 'function') {
      this.getSpecFromEl(MK.getSpec(), el, function(spec) {
        var parentFlow = MK.getSpec().dynamic.flows[spec.flowIndex];
        /*Hledání otce <tr> a označení pro smazání data-del*/
        if (parentFlow) {
          var parent = parentFlow['Datum zaúčtování transakce'].domel.parentElement.parentElement;//+2 levely výše
          var datr = parent.getAttribute('data-del');
          if (datr) { //odznačení pro smazání
            parent.removeAttribute('data-del');
          } else { //označení pro smazání
            parent.setAttribute('data-del', 'true');
          }
        }
      });
    }
  };
  
  MK.Editor.prototype.changeEdmode = function changeEdmode(mode) {
    var self = this;
    var edels = this.view.ifr.contentDocument.querySelectorAll('[data-status]');
    this.editingmode = mode;
    //this.view.clearEl('#msgs');
    this.view.msgs.style.display = 'block';
    this.view.msgs.innerHTML = this.emodes[mode].msg;
    /**události pro editor**/
    var modifyEv = function modEv(e) { self.modify(this); };
    var addEv = function addEv(e) { self.add(this); };
    var delEv = function delEv(e) { self.del(this); };
    var updateEv = function updateEv() { self.update(); };
    
    this.remAttrs('contenteditable'); //clear ce kdyz bylo editováno
    this.view.recalc.onclick = updateEv; //update na tlačítko
    this.view.ifr.contentDocument.documentElement.setAttribute('data-mode', mode);
    
    for (var i = 0; i < edels.length; i++) {
      edels[i].onclick = '';
      edels[i].onblur = '';
      if(mode === 'modify') {
        edels[i].onclick = modifyEv;
        edels[i].onblur = updateEv;
      } else if (mode === 'add') {
        edels[i].onclick = addEv;
      } else if (mode === 'del') {
        edels[i].onclick = delEv;
      }
    }
  };
  
  MK.Editor.prototype.update = function update() {
    var remels = this.view.ifr.contentDocument.querySelectorAll('[data-del]');
    if (remels.length > 0) {
      for (var i = 0; i < remels.length; i++) {
        remels[i].parentNode.removeChild(remels[i]);
      }
    }

    this.editOff();
    this.editOn();
    this.remAttrs('contenteditable'); //mazani contenteditablu
  };
  
  MK.Editor.prototype.editOn = function editOn() {
    this.editing = true;
    this.view.loadcss('css/iframe.css'); //injekce css
    this.view.editmod.value = 'Hotovo';
    this.view.parser.parseDOM(); //validace formátu obsahu
    this.view.parser.recalculate(); //validace logiky a výpočty
    this.view.edmode.onchange(); //aktivace volby editmodu na selectu
    this.view.editorel.style.display = 'block';
    if (this.view.parser.errors > 0) {
      this.view.msgDomErr(); //zobrazit počet chyb
    } else {
      this.view.renderLog({type: 'msg', text: 'Dokument byl zpracován v pořádku. Transakce byly přepočítány. Tlačítkem <b>Hotovo</b> ukončíte editaci.'}, true);
      this.view.msgs.style.display = 'block';
      this.view.msgs.innerHTML = this.emodes[this.editingmode].msg;
    }
  };
  
  MK.Editor.prototype.editOff = function editOff() {
    this.editing = false;
    this.view.unloadcss(); //vymazat injektované css
    this.view.editmod.value = 'Editovat';
    this.editingmode = null;
    this.view.parser.errors = 0;
    var bubles = this.view.ifr.contentDocument.querySelectorAll('[data-status]');
    /*reset událostí data elementů*/
    for (var i = 0; i < bubles.length; i++) {
      var b = bubles[i];
      b.onmouseover = b.onmouseout = b.onblur = b.onclick = '';
    }

    this.remAttrs(['data-status', 'data-flow', 'data-mode']);
    this.view.clearEls(['msgs', 'logmsg', 'logerr']);
    this.view.renderLog({type: 'msg', text: 'Dokument ze ouboru: <b>'+ this.view.loadedFile.name +'</b> je načten do editoru. Tlačítkem <b>Editovat</b> zpracujete dokument a můžete upravovat.'});
    this.view.editorel.style.display = 'none';
  };
  
  /*vygeneruje nový DOM element transakce <tr>*/
  MK.Editor.prototype.addNewFlowTrel = function addNewFlowTrel(afel, sdate, acdate, flowId) {
    var html = '\n'; //all HTML for new line
    var ntr = ''; //HTML pro nazev nove transakce
    var br = ''; //bez zalomení nebo se zalobením
    var nobrsp = ''; //bez odsazení tagu <nobr> nebo s odsazením
    var tdsp = '                '; //odsazení tagu <td> nebo s odsazením
    var trsp = '              '; //odsazení pro tag <tr>              
    var dlparams = ''; //paramaetry elementů datových položek vlevo od názvu a popisu
    var nparams = ''; //paramaetry elementu názvu a popisu
    var rparams = ''; //paramaetry elementů datových položek vpravo od názvu a popisu
    
    if(MK.stType === 'acmonth') {
      nobrsp = '                  '; //add nobr spaces
      ntr = nobrsp + '<u>NOVÁ TRANSAKCE</u>';
      br = '\n'; //s zalomenými řádky
      dlparams = 'align="center" valign="top"';
      nparams = 'align="left" valign="top"';
      rparams = 'align="right" valign="top"';
      
      html += br;
      html += tdsp + '<td ' + dlparams + 'contenteditable="true">\n';
      html += nobrsp + '<nobr>' + flowId + '</nobr>\n';
      html += tdsp + '</td>\n';
    } else if(MK.stType === 'acrange') {
      ntr = 'NOVÁ TRANSAKCE';
      dlparams = 'class="data" align="center"';
      nparams = 'class="data"';
      rparams = 'class="data" align="right"';
      html += br;
    }
    
    //html += br;
    html += tdsp + '<td ' + dlparams + 'contenteditable="true">' + br;
    html += nobrsp + '<nobr>' + sdate + '</nobr>' + br;
    html += tdsp + '</td>\n';
    html += tdsp + '<td ' + dlparams + 'contenteditable="true">' + br;
    html += nobrsp + '<nobr>' + acdate + '</nobr>' + br;
    html += tdsp + '</td>\n';
    html += tdsp + '<td ' + nparams + 'contenteditable="true">' + br;
    html += ntr + br;
    html += nobrsp + '<br>Nějaký popis<br>může být rozdělen do více řádků..' + br;
    html += nobrsp + '</td>\n';
    html += tdsp + '<td ' + rparams + 'contenteditable="true">' + br;
    html += nobrsp + '<nobr>0,00</nobr>' + br;
    html += tdsp + '</td>\n';
    html += tdsp + '<td ' + rparams + 'contenteditable="true">' + br;
    html += nobrsp + '<nobr>0,00</nobr>' + br;
    html += tdsp + '</td>' + br + trsp;
    
    var trel = document.createElement('tr');
    trel.innerHTML = html;
    //udělat správné odsazení
    var idnt = this.view.ifr.contentDocument.createTextNode(br + trsp);
    var ins = afel.parentElement.insertBefore(trel, afel.nextElementSibling);
    ins.parentElement.insertBefore(idnt, ins.nextElementSibling); //add \n && indent space
    //console.log(html);
  };
  
})();